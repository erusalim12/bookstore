﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Abstract;
using WebUI.Models;

namespace WebUI.Controllers
{
    public class BooksController : Controller
    {
        private IBookRepository repository;
#warning количество элементов на страницу!
        public int pageSize = 2;

        public BooksController(IBookRepository repo)
        {
            repository = repo;
        }

        public ViewResult List(string genre,int page = 1)
        {
            BookListViewModel model = new BookListViewModel
            {
                Books = repository.Books
                .Where(b=>genre==null||b.Genre==genre)
                .OrderBy(book=>book.Id)
                .Skip((page-1)*pageSize)
                .Take(pageSize),

                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = pageSize,
                    TotalItems = genre==null ? 
                        repository.Books.Count() :
                        repository.Books.Count(b => b.Genre==genre)
                },
                CurrentGenre=genre
            };
            return View(model);

        }
    }
}