﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Abstract;
using Domain.Entities;

namespace WebUI.Controllers
{
  [Authorize]
    public class AdminController : Controller
    {
        IBookRepository repository;

        public AdminController(IBookRepository repo)
        {
            repository = repo;
        }
        public ViewResult Index()
        {
            return View(repository.Books);
        }

        public ViewResult Edit(int id)
        {
            Book book = repository.Books.FirstOrDefault(b => b.Id == id);
            return View(book);
        }

        [HttpPost]
        public ActionResult Edit(Book book,HttpPostedFileBase file=null)
        {
            if (ModelState.IsValid)
            {
                repository.SaveBook(book, file);
                TempData["message"] = string.Format("Изменения информации о книге \"{0}\" сохранены", book.Name);
                return RedirectToAction("Index");
            }
            else
            {
                return View(book);
            }
        }

        public ViewResult Create()
        {
            return View(new Book());
        }


        [HttpPost]
        public ActionResult Create(Book book, HttpPostedFileBase file = null)
        {
            if (ModelState.IsValid)
            {
                repository.SaveBook(book,file);
                TempData["message"] = string.Format("Книга \"{0}\" успешно добавлена!", book.Name);
                return RedirectToAction("Index");
            }
            else
            {
                return View(book);
            } 
        }

        public ActionResult Delete(int id = 0)
        {
            repository.DeleteBook(id);
            return RedirectToAction("Index");
        }
    }
}