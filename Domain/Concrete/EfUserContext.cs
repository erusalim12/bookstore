﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Domain.Abstract;
using Domain.Entities;

namespace Domain.Concrete
{
    class EfUserContext : IUserRepository
    {
        EfDbContext context = new EfDbContext();

        public IEnumerable<User> Users
        {
            get { return context.Users; }
        }

        public void CreateUser(User user)
        {
            var dbEntry = context.Users.FirstOrDefault(u => u.Login == user.Login);
            if (dbEntry == null)
            {
                //новый пользователь
                context.Users.Add(user);
            }
            //обновление пользователя
            else
            {
                dbEntry.Login = user.Login;
                dbEntry.Age = user.Age;
                dbEntry.Password = user.Password;
            }

            //закрепляем изменения в бд
            context.SaveChanges();
        }

        public void RemoveUser(int userId)
        {

            var dbEntry = context.Users.Find(userId);
           
            if (dbEntry != null)
            {
                context.Users.Remove(dbEntry);
            }

            context.SaveChanges();
        }
    }
}
