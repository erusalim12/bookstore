﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Domain.Abstract;
using Domain.Entities;
using System.Web;
using System.IO;
using System.Web.Hosting;


namespace Domain.Concrete
{
    public class EFBookRepository:IBookRepository
    {
         EfDbContext context = new  EfDbContext();

        public IEnumerable<Book> Books
        {
            get { return context.Books; }
        }

        public void SaveBook(Book book, HttpPostedFileBase postedFile)
        {
            if (book.Id == 0)//сохраняем новую
            {
                if (IsImage(postedFile))//если файл изображение
                {
                    var fileName = Path.GetFileName(postedFile.FileName);//получили название файла
                    book.ImagePath = Path.Combine(HostingEnvironment.MapPath("~/Images"), fileName);//создали путь к файлу и сохранили его в записи объекта
                    postedFile.SaveAs(book.ImagePath);//записали файл по этому пути
                }

                context.Books.Add(book);
            }
            else//редактируем
            {
                Book dbEntry = context.Books.Find(book.Id);
                if (dbEntry != null)
                {
                    dbEntry.Name = book.Name;
                    dbEntry.Author = book.Author;
                    dbEntry.Description = book.Description;
                    dbEntry.Genre = book.Genre;
                    dbEntry.Price = book.Price;
                    if (postedFile != null) //если файл добавили
                    {
                        if (IsImage(postedFile)) //если файл изображение
                        {
                            var fileName = Path.GetFileName(postedFile.FileName); //получили название файла

                            var path = Path.Combine(HostingEnvironment.MapPath("~/Images"), fileName); //путь к файлу
                            postedFile.SaveAs(path); //сохранили файл

                            dbEntry.ImagePath = path; //записали расположение фото
                        }
                    }
                    else
                    {
                       // dbEntry.ImagePath = book.ImagePath;
                    }
               
                }
            }
            context.SaveChanges();
        }

        public void DeleteBook(int bookId)
        {
            if (bookId != 0)
            {
                Book dbEntry = context.Books.Find(bookId);
                if (dbEntry != null)
                {
                    context.Books.Remove(dbEntry);
                }
            }
            context.SaveChanges();
        }


           #region //проферка файла на изображение

        private bool IsImage(HttpPostedFileBase postedFile)
        {
            try
            {
                using (var bitmap = new Bitmap(postedFile.InputStream))
                {
                    return !bitmap.Size.IsEmpty;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion
    }
}