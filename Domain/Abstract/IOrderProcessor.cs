﻿using Domain.Entities;

namespace Domain.Abstract
{
    public interface IOrderProcessor
    {
        void processOrder(Cart cart, ShippingDetails shippingDetails);
    }
}