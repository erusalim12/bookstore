﻿ using System.Collections.Generic;
using System.Web;
using Domain.Entities;

namespace Domain.Abstract
{
    public interface IBookRepository
    {
        IEnumerable<Book> Books { get; }

        void SaveBook(Book book,HttpPostedFileBase postedFile);

        void DeleteBook(int bookId);
    }
}