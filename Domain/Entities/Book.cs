﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Domain.Entities
{
    public class Book
    {
        [HiddenInput(DisplayValue = false)]
        [Display(Name = "ID")]
        public int Id { get; set; }

        [Display(Name = "Название")]
        [Required(ErrorMessage = "Введите название книги")]
        public string Name { get; set; }
          [Display(Name = "Автор")]
          [Required(ErrorMessage = "Введите автора книги")]
        public string Author { get; set; }
        [DataType(DataType.MultilineText)]
          [Display(Name = "Описание")]
          [Required(ErrorMessage = "Введите описание книги")]

        public string Description { get; set; }

          [Display(Name = "Жанр")]
          [Required(ErrorMessage = "Укажите жанр книги")]
        public string Genre { get; set; }

          [Display(Name = "Цена (руб)")]
          [Required]
          [Range((float)decimal.Zero,float.MaxValue,ErrorMessage = "Минимальная цена 0.01 руб.")]
     
        public decimal Price { get; set; }

          public string ImagePath { get; set; }
    }
}
