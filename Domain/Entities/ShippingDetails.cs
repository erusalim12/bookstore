﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Entities
{
    public class ShippingDetails
    {
        [Required(ErrorMessage ="Укажите ваше имя" )]
        public string Name { get; set; }
         [Required(ErrorMessage = "Адрес доставки")]
      [Display(Name = "Первый адрес")]
        public string Line1 { get; set; }
         [Display(Name = "Электронная почта")]
        public string Line2 { get; set; }
         [Display(Name = "Третий адрес")]
        public string LIne3 { get; set; }
         [Required(ErrorMessage = "Укажите ваш город")]
         [Display(Name = "Город")]
        public string City { get; set; }
         [Required(ErrorMessage = "Страна проживания")]
         [Display(Name = "Страна")]
        public string Country { get; set; }
        public bool GiftWrap { get; set; }
    }
}