﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Domain.Abstract;
using Domain.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using WebUI.Controllers;
using WebUI.Models;

namespace UnitTests
{
    [TestClass]
   public  class CartTest
    {
        [TestMethod]
        public void Can_add_New_Lines()
        {
            //организация
            Book book1 = new Book {Id = 1, Name = "Book1"};
            Book book2 = new Book {Id = 2, Name = "Book2"};


            Cart cart = new Cart();
            cart.AddItem(book1,1);
            cart.AddItem(book2,1);

            //действие
            List<CartLine> result = cart.Lines.ToList();


            //утверждение
            Assert.AreEqual(result.Count(),2);
            Assert.AreEqual(result[0].Book,book1);
            Assert.AreEqual(result[1].Book,book2);

        }
          [TestMethod]
        public void Cam_add_Quantity_For_Existing_Lines()
        {
            //организация
            Book book1 = new Book { Id = 1, Name = "Book1" };
            Book book2 = new Book { Id = 2, Name = "Book2" };


            Cart cart = new Cart();
            cart.AddItem(book1, 1);
            cart.AddItem(book2, 1);
            cart.AddItem(book1, 5);

            //действие
            List<CartLine> result = cart.Lines.OrderBy(c => c.Book.Id).ToList();


            //утверждение
            Assert.AreEqual(result.Count(), 2);
            Assert.AreEqual(result[0].Quantity, 6);
            Assert.AreEqual(result[1].Quantity, 1);

        }

          [TestMethod]
          public void Can_Remove_Line()
          {
              //организация
              Book book1 = new Book { Id = 1, Name = "Book1" };
              Book book2 = new Book { Id = 2, Name = "Book2" };
              Book book3 = new Book { Id = 3, Name = "Book3" };


              Cart cart = new Cart();

              //действие
              cart.AddItem(book1, 1);
              cart.AddItem(book2, 1);
              cart.AddItem(book1, 5);
              cart.AddItem(book3, 2);
              cart.RemoveLine(book2);

             
             // List<CartLine> result = cart.Lines.OrderBy(c => c.Book.Id).ToList();


              //утверждение
         Assert.AreEqual(cart.Lines.Count(c => c.Book==book2), 0);
              Assert.AreEqual(cart.Lines.Count(),2);

          }


        [TestMethod]
          public void Calculate_Cart_Total()
          {
              //организация
              Book book1 = new Book { Id = 1, Name = "Book1",Price = 100};
              Book book2 = new Book { Id = 2, Name = "Book2" ,Price = 55};
              Book book3 = new Book { Id = 3, Name = "Book3" };


              Cart cart = new Cart();

              //действие
              cart.AddItem(book1, 1);
              cart.AddItem(book2, 1);
              cart.AddItem(book1, 5);
            decimal result = cart.ComputateTotalValue();


              // List<CartLine> result = cart.Lines.OrderBy(c => c.Book.Id).ToList();


              //утверждение
            Assert.AreEqual(result, 655);

          }


        [TestMethod]
        public void Can_Clear_cart()
        {
            //организация
            Book book1 = new Book { Id = 1, Name = "Book1", Price = 100 };
            Book book2 = new Book { Id = 2, Name = "Book2", Price = 55 };
     


            Cart cart = new Cart();

            //действие
            cart.AddItem(book1, 1);
            cart.AddItem(book2, 1);
            cart.AddItem(book1, 5);
           cart.Clear();


            // List<CartLine> result = cart.Lines.OrderBy(c => c.Book.Id).ToList();


            //утверждение
            Assert.AreEqual(cart.Lines.Count(), 0);

        }

        [TestMethod]
        public void Can_Add_To_Cart()
        {
            //организация
           var mock = new Mock<IBookRepository>();
            mock.Setup(m => m.Books).Returns(new List<Book>
            {
                new Book {Id = 1, Name = "book1", Genre = "Genre1"}
            }.AsQueryable());

            

            Cart cart = new Cart();
            CartController controller = new CartController(mock.Object,null);

            //утверждение
            controller.AddToCart(cart, 1, null);

            Assert.AreEqual(cart.Lines.Count(), 1);
            Assert.AreEqual(cart.Lines.ToList()[0].Book.Id, 1);
        }

        [TestMethod]
        public void Adding_Book_To_cart_And_Goes_To_Index()
        {
            //организация
            var mock = new Mock<IBookRepository>();
            mock.Setup(m => m.Books).Returns(new List<Book>
            {
                new Book {Id = 1, Name = "book1", Genre = "Genre1"}
            }.AsQueryable());



            Cart cart = new Cart();
            CartController controller = new CartController( mock.Object,null);

            //утверждение
            RedirectToRouteResult result = controller.AddToCart(cart, 2, "myUrl");

            Assert.AreEqual(result.RouteValues["action"], "Index");
            Assert.AreEqual(result.RouteValues["returnUrl"], "myUrl");
        }

        [TestMethod]
        public void Can_View_Cart_Correct()
        {
        

            Cart cart = new Cart();
            CartController controller = new CartController(null,null);


            CartIndexViewModel result = (CartIndexViewModel) controller.Index(cart, "myUrl").ViewData.Model;
            //утверждение
           

            Assert.AreEqual(result.Cart, cart);
            Assert.AreEqual(result.ReturnUrl, "myUrl");
        }


        [TestMethod]
        public void Cannot_Checout_Empty_Cart()
        {
            var mock = new Mock<IOrderProcessor>();
            Cart cart = new Cart();
            ShippingDetails shippingDetails = new ShippingDetails();

            CartController controller =new CartController(null,mock.Object);

            ViewResult result = controller.Checkout(cart, shippingDetails);

            mock.Verify(m=>m.processOrder(It.IsAny<Cart>(),It.IsAny<ShippingDetails>()),Times.Never());


            Assert.AreEqual("",result.ViewName);
            Assert.AreEqual(false,result.ViewData.ModelState.IsValid);
        }

        [TestMethod]
        public void Cannot_Checout_invalid_ShippingDetails()
        {
            var mock = new Mock<IOrderProcessor>();
            Cart cart = new Cart();
            cart.AddItem(new Book(), 1);

            CartController controller = new CartController(null, mock.Object);
            controller.ModelState.AddModelError("error","error");
            ViewResult result = controller.Checkout(cart, new ShippingDetails());

            mock.Verify(m => m.processOrder(It.IsAny<Cart>(), It.IsAny<ShippingDetails>()), Times.Never());


            Assert.AreEqual("", result.ViewName);
            Assert.AreEqual(false, result.ViewData.ModelState.IsValid);
        }



        [TestMethod]
        public void Cannot_Checout_And_Submit_Order()
        {
            var mock = new Mock<IOrderProcessor>();
            Cart cart = new Cart();
            cart.AddItem(new Book(), 1);

            CartController controller = new CartController(null, mock.Object);
      
            ViewResult result = controller.Checkout(cart, new ShippingDetails());

            mock.Verify(m => m.processOrder(It.IsAny<Cart>(), It.IsAny<ShippingDetails>()), Times.Once());


            Assert.AreEqual("Complete", result.ViewName);
            Assert.AreEqual(true, result.ViewData.ModelState.IsValid);
        }



    }
}
