﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Domain.Abstract;
using Domain.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using WebUI.Controllers;
using WebUI.Models;

namespace UnitTests
{   
    [TestClass]
  public class AdminTests
    {
        [TestMethod]
        public void Index_Contains_All_Books()
        {
            Mock<IBookRepository> mock = new Mock<IBookRepository>();
            mock.Setup(m => m.Books).Returns(new List<Book>
            {
                new Book {Id = 1, Name = "Book1"},
                new Book {Id = 2, Name = "Book2"},
                new Book {Id = 3, Name = "Book3"},
                new Book {Id = 4, Name = "Book4"},
                new Book {Id = 5, Name = "Book5"}
            });
            AdminController controller = new AdminController(mock.Object);


            List<Book> result = ((IEnumerable<Book>) controller.Index().ViewData.Model).ToList();


            Assert.AreEqual(result.Count(), 5);
            Assert.AreEqual(result[0].Name, "Book1");
            Assert.AreEqual(result[1].Name, "Book2");
        }

        [TestMethod]
        public void Can_Edit_Books()
        {
            Mock<IBookRepository> mock = new Mock<IBookRepository>();
            mock.Setup(m => m.Books).Returns(new List<Book>
            {
                new Book {Id = 1, Name = "Book1"},
                new Book {Id = 2, Name = "Book2"},
                new Book {Id = 3, Name = "Book3"},
                new Book {Id = 4, Name = "Book4"},
                new Book {Id = 5, Name = "Book5"}
            });
            AdminController controller = new AdminController(mock.Object);


          //действие(act)
            Book book1 = controller.Edit(1).ViewData.Model as Book;
            Book book2 = controller.Edit(2).ViewData.Model as Book;
            Book book3 = controller.Edit(3).ViewData.Model as Book;

            Assert.AreEqual(1, book1.Id);
            Assert.AreEqual(2, book2.Id);
            Assert.AreEqual(3, book3.Id);
        }


        [TestMethod]
        public void Canont_Edit_Nonexistent_Book()
        {
            Mock<IBookRepository> mock = new Mock<IBookRepository>();
            mock.Setup(m => m.Books).Returns(new List<Book>
            {
                new Book {Id = 1, Name = "Book1"},
                new Book {Id = 2, Name = "Book2"},
                new Book {Id = 3, Name = "Book3"},
                new Book {Id = 4, Name = "Book4"},
                new Book {Id = 5, Name = "Book5"}
            });
            AdminController controller = new AdminController(mock.Object);


            //действие(act)
            Book result = controller.Edit(7).ViewData.Model as Book;

            Assert.IsNull(result);
        }


        [TestMethod]
        public void Can_Save_Valid_Changes()
        {
            Mock <IBookRepository> mock = new Mock<IBookRepository>();
            AdminController controller = new AdminController(mock.Object);

            Book book = new Book{Name = "Test",};

            ActionResult result = controller.Edit(book);


            mock.Verify(m=>m.SaveBook(book,null));

            Assert.IsNotInstanceOfType(result,typeof(ViewResult));



        }

        [TestMethod]
        public void Cannot_Save_InValid_Changes()
        {
            Mock<IBookRepository> mock = new Mock<IBookRepository>();
            AdminController controller = new AdminController(mock.Object);

            Book book = new Book { Name = "Test" };

            controller.ModelState.AddModelError("error", "error");


            ActionResult result = controller.Edit(book,null);


            mock.Verify(m => m.SaveBook(It.IsAny<Book>(),null), Times.Never());

            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }
    }
}
